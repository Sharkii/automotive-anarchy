using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragandtest : MonoBehaviour
{

    public GameObject selectedObject;
    public Vector3 offset;
    public Vector3 mousePosition;
    // Start is called before the first frame update
    private Vector2 originalPosition;
    public GameObject tireCollision;
    public GameObject wheel_axel;
    public GameObject selected_object;
    public float distance = Vector3.Distance(object1.transform.position, object2.transform.position);
    void Awake()
    {
        originalPosition = transform.position;
    }


    void Start()
    {
        
    }


    // Credit to Gamedevbeginner.com for baseline code
    // Update is called once per frame
    void Update()
    {
      

        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 1;

        if (Input.GetMouseButtonDown(0))
        {
            Collider2D targetObject = Physics2D.OverlapPoint(mousePosition);


            if (targetObject)
            {
                Debug.Log("Click!");
                selectedObject = targetObject.transform.gameObject;
                offset = selectedObject.transform.position - mousePosition;
            }
            
        }
        if(selectedObject)
        {
            selectedObject.transform.position = mousePosition + offset;
        }
        if(Input.GetMouseButtonUp(0) && selectedObject)
        {   if (distance > 3) ;
            {
                transform.position = tireCollision.transform.position;
            }
            //put back where it was originally placed 
           selectedObject.transform.position = originalPosition;
            selectedObject = null;
        }


    }

}